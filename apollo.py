#VERSION: 1.0
#AUTHORS: risq

credentials = {
    "username": "your_username",
    "password": "your_password",
}

import tempfile
import os
from re import compile as re_compile
from re import DOTALL
from requests import session
from novaprinter import prettyPrinter
from helpers import download_file, retrieve_url

try:
    from HTMLParser import HTMLParser
    unescape = HTMLParser().unescape
except ImportError:
    import html
    unescape = html.unescape

table_re = re_compile(r"<table class=\"torrent_table(.*)<\/table>", DOTALL)
rows_re = re_compile(r"<tr.*?<\/tr>", DOTALL)
group_title_re = re_compile(r"<div class=\"group_info clear\">(.*?)<span", DOTALL)
release_re = re_compile(r"<tr class=\"group_torrent .* edition[\s\"].*<\/a>(.*?)<\/strong><\/td>", DOTALL)
torrent_re = re_compile(r"<a href=\"(torrents\.php\?action=download.*?)\".*<\/a>.*<a href=\"(torrents\.php\?id=.*?)\">(.*?)<\/a>.*<td class=\"number_column nobr\">(.*?)<\/td>.*<td class=\"number_column\">(.*?)<\/td>.*<td class=\"number_column\">(.*?)<\/td>", DOTALL)

tag_re = re_compile(r"<[^>]+>")
comma_re = re_compile(r",")

class apollo(object):
    url = "https://apollo.rip"
    name = "Apollo"

    def __init__(self):
        with session() as c:
            c.post(self.url + '/login.php', data = credentials)
            self.c = c

    def download_torrent(self, url):
        file, path = tempfile.mkstemp()
        file = os.fdopen(file, "wb")
        response = self.c.get(url)
        file.write(response.content)
        file.close()
        print(path + " " + url)

    def format(self, text):
        return unescape(tag_re.sub("", text)).strip()

    def search(self, what, cat="all"):
        query = self.url + "/torrents.php?searchstr=" + what + "&group_results=1&action=advanced&searchsubmit=1"
        response = self.c.get(query)
        data = response.text

        table = table_re.search(data).group(0)
        rows = rows_re.findall(table)

        title = ""
        release = ""
        format_data = ""
        dl_link = ""
        link = ""
        size = ""
        seeds = ""
        leech = ""

        for row in rows:
            if row.startswith("<tr class=\"group\"") or row.startswith("<tr class=\"group "):
                title = self.format(group_title_re.search(row).group(1))
            elif row.startswith("<tr class=\"group_torrent"):
                release_info = release_re.search(row)
                if release_info:
                    release = self.format(release_info.group(1))
                else:
                    torrent = torrent_re.search(row)

                    dl_link = self.format(torrent.group(1))
                    link = self.format(torrent.group(2))
                    format_data = self.format(torrent.group(3))
                    size = comma_re.sub("", torrent.group(4))
                    seeds = comma_re.sub("", torrent.group(5))
                    leech = comma_re.sub("", torrent.group(6))

                    current_item = dict()
                    current_item["link"] = self.url + '/' + dl_link
                    current_item["name"] = title + ' - ' + release + ' [' + format_data + ']'
                    current_item["size"] = size
                    current_item["seeds"] = seeds
                    current_item["leech"] = leech
                    current_item["desc_link"] = self.url + '/' + link
                    current_item["engine_url"] = self.url

                    prettyPrinter(current_item)

        return
