qBittorrent Gazelle Search
==========================

*qBittorrent search provider for gazelle-based trackers.*

## Installation
- [Download the archive](https://gitlab.com/zdk9Z9GF5p6w/qbittorent-gazelle-search/repository/archive.zip?ref=master)
- Extract the archive, then open `passtheheadphones.py` and `apollo.py` with your favorite text editor. Replace `your_username` and `your_password` entries with your credentials.
- Move both files into qBittorent search plugins location :
  - Linux: `~/.local/share/data/qBittorrent/nova3/engines/`
  - Windows: `%localappdata%\qBittorrent\nova3\engines\`
  - OS X: `~/Library/Application Support/qBittorrent/nova3/engines/`
- Restart qBittorrent.
- To display the search engine tab, check `Search Engine` under qBittorent `View` menu. Click on the `Search plugins...` button on the bottom right of the window. Both plugins should be displayed in the list.

## Troubleshooting
If the search engines are not displayed in the plugins list, you may need to install `requests`:
- Using *pip*: `pip install requests`
- Using *easy_install*: on windows, open a terminal, navigate to the `Scripts` folder inside your python install directory (`C:\Python27\Scripts`). Enter `easy_install.exe requests`. (see http://stackoverflow.com/a/17309309/2320186).
- From source: see http://docs.python-requests.org/en/master/user/install/.
